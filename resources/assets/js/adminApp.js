
var CkeditorCondigCustom = {
			    filebrowserImageBrowseUrl: baseWebSiteURL+'/laravel-filemanager?type=Images',
			    filebrowserImageUploadUrl: baseWebSiteURL+'/laravel-filemanager/upload?type=Images&_token='+$("input[name='_token']").val(),
			    filebrowserBrowseUrl: baseWebSiteURL+'/laravel-filemanager?type=Files',
			    filebrowserUploadUrl: baseWebSiteURL+'/laravel-filemanager/upload?type=Files&_token='+$("input[name='_token']").val()
};




$(function(){

	//Revisar si hay un boton de cancelar registro
	urlCancelButton();

	$.ajaxSetup({
	    headers: {
	        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
	    }
	});


});


function addLoader()
{
	var overlay="<div id=\"overlay_admin_ajax\" class=\"overlay\"><i class=\"fas fa-3x fa-sync-alt fa-spin\"></i></div>";

	$("body").append(overlay);
	console.log("Add Loader");
}


function removeLoader()
{
	$("#overlay_admin_ajax").remove();

	console.log("Remove Loader");
}


function urlCancelButton()
{

	if($(".btn-cancel-button-redirect").length>0)
	{
		$(".btn-cancel-button-redirect").click(function(){
			alertify.confirm('Consola de Administración', '¿Estas seguro de abandonar la ventana actual?', 
				function()
				{ 
					var url=$(".btn-cancel-button-redirect").data("url");

					document.location=url;

				},
				function()
	        	{ 

	        	}
	        );
		});
		
	}

}



function urlDeleteButtonModule(classButtonDelete=".btn-primary-delete-module-select")
{
	if($(classButtonDelete).length>0)
		{
			$(classButtonDelete).click(function(e){
				e.preventDefault();

				var ObjClickButton=$(this);

				alertify.confirm('Consola de Administración', '¿Estas seguro de borrar el registro?', 
					function()
					{ 
						

						$.ajax({
							
							url: ObjClickButton.attr("href"),

							method:'get',
							async :true,
							cache: false,
							//data:$("#form-control").serialize(),
							beforeSend:function(){
								addLoader();
							}
						}).done(function(data){
							removeLoader();
							if(data.error=="success")
							{
								alertify.set('notifier','position', 'top-right');
								alertify.success("Registro Borrado de manera exitosa");

								setTimeout(function(){
									//Si fue un exito borramos
									$("#rows-module").DataTable().ajax.reload();

								}, 1000);


							}
							else if(data.error=="error")
							{
								alertify.set('notifier','position', 'top-right');
								alertify.error(data.mensaje);
							}
							else
							{
								alertify.set('notifier','position', 'top-right');
								alertify.error(data.mensaje);
							}



						}).fail(function(data){
							removeLoader();
							alertify.error('No fue posible borrar el registro intente de nuevo');
						});

					},
					function()
		        	{ 

		        	}
		        );
			});
			
		}


}


function activatePemsExtras()
{
	if($(".input-text-perms-checks").length>0)
	{

		$(".input-text-perms-checks").click(function(){

			if($(this).is(":checked"))
			{


				$("."+$(this).attr("data-classChilds") ).prop( "checked", true );

			}
			else
			{
				$("."+$(this).attr("data-classChilds") ).prop( "checked", false );
			}

		});

	}


}






















