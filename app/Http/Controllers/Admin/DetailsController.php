<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\JobEntry as JobEntryModel;
use App\Models\Repair as RepairModel;
use App\Models\Module;

class DetailsController extends Controller
{
    private $sNameModule;
    private $iIdModule;
    private $rowModule;

    public function __construct()
    {
        $this->iIdModule = 4;
        $this->rowModule = Module::find($this->iIdModule);
        $this->sNameModule = $this->rowModule->name;
    }

    public function getDetails($id)
    {
        try
        {
            $oJobEntry = JobEntryModel::where('id', $id)->firstOrFail();
            $oRepairs = RepairModel::where('job_id', $id)->get();
            
            return view("admin.{$this->sNameModule}.details", [
                'iIdModule' => $this->iIdModule,
                'sNameModule' => $this->sNameModule,
                'sNameTitle' => $this->rowModule->title,
                'oJob' => $oJobEntry,
                'oRepairs' => $oRepairs,
                'id' => $id,
            ]);
        }
        catch(Exception $e)
        {
            return abort(404, 'El registro no se encuentra en la base de datos!');
        }
    }

    public function postDetails(Request $request, $id)
    {
        $columns = array(
            'id',
            'service_id',
            'price',
            'user_id',
            'created_at',
            'id'
        );

        $totalData = RepairModel::where('job_id', $id)->count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $oRepairs = RepairModel::offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->where('job_id', $id)->get();
        }
        else
        {
            $search = $request->input('search.value');

            $oRepairs = RepairModel::where('id', 'LIKE', "%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->where('job_id', $id)->get();

            $totalFiltered = RepairModel::where('id', 'LIKE', "%{$search}%")
                                    ->where('job_id', $id)->get()
                                    ->count();
        }

        $data = array();

        if(!empty($oRepairs))
        {
            foreach($oRepairs as $oRepair)
            {

                $nestedData['id'] = $oRepair->id;
                $nestedData['service_id'] = $oRepair->Service->name;
                $nestedData['price'] = "$ ".$oRepair->price;
                $nestedData['user_id'] = $oRepair->User->name;
                $date = date("M d Y H:i:s", strtotime($oRepair->created_at));
                $nestedData['created_at'] = $date;

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            'draw'              => intval($request->input('draw')),
            'recordsTotal'      => intval($totalData),
            'recordsFiltered'   => intval($totalFiltered),
            'data'              => $data
        );

        return $json_data;
    }
}
