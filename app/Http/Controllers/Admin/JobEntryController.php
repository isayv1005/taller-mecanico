<?php

namespace App\Http\Controllers\Admin;


use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\Module;
use App\Models\JobEntry as JobEntryModel;
use App\Models\Car as CarModel;
use App\Models\Status as StatusModel;
use App\Models\CarPart as CarPartModel;
use App\Models\Responsible as ResponsibleModel;
use App\Models\Repair as RepairModel;
use Auth;
use DB;
use Log;
use App\Http\Controllers\Admin\TestPdfController;

class JobEntryController extends Controller
{
    private $sNameModule;
    private $iIdModule;
    private $rowModule;

    public function __construct()
    {
        $this->iIdModule = 4;
        $this->rowModule = Module::find($this->iIdModule);
        $this->sNameModule = $this->rowModule->name;
    }

    public function getIndex()
    {   
        return view("admin.{$this->sNameModule}.index", [
            'iIdModule' => $this->iIdModule,
            'sNameModule' => $this->sNameModule,
            'sNameTitle' => $this->rowModule->title
        ]);
    }

    public function postRows(Request $request)
    {
        $columns = array(
            'id',
            'status_id',
            'car_plates',
            'car_brand',
            'car_version',
            'car_model',
            'id'
        );

        $totalData = JobEntryModel::count();
        $totalFiltered = $totalData;

        $limit = $request->input('length');
        $start = $request->input('start');
        $order = $columns[$request->input('order.0.column')];
        $dir = $request->input('order.0.dir');

        if(empty($request->input('search.value')))
        {
            $oJobs = JobEntryModel::offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->get();
        }
        else
        {
            $search = $request->input('search.value');

            $oJobs = JobEntryModel::where('car_plates', 'LIKE', "%{$search}%")
                            ->orWhere('car_version', 'LIKE', "%{$search}%")
                            ->offset($start)
                            ->limit($limit)
                            ->orderBy($order, $dir)
                            ->get();

            $totalFiltered = JobEntryModel::where('car_plates', 'LIKE', "%{$search}%")
                                    ->where('car_version', 'LIKE', "%{$search}%")
                                    ->count();
        }

        $data = array();

        if(!empty($oJobs))
        {
            foreach($oJobs as $oJob)
            {
                $edit = null;
                $delete = null;
                $details = url("admin/{$this->sNameModule}/details/{$oJob->id}");
                $note = null;
                $repair = null;

                if(Auth::user()->hasPermIntern($this->iIdModule, 'edit'))
                {
                    if($oJob->status_id < 2)
                    {
                        $edit = url("admin/{$this->sNameModule}/edit/{$oJob->id}");
                    }
                }

                if(Auth::user()->hasPermIntern($this->iIdModule, 'delete'))
                {
                    $delete = url("admin/{$this->sNameModule}/delete/{$oJob->id}");
                }

                if($oJob->status_id <= 2)
                {
                    $repair = url("admin/{$this->sNameModule}/workshop/{$oJob->id}");
                }

                if($oJob->status_id === 2)
                {
                    $note = url("admin/generate-note/{$oJob->id}");
                }

                $nestedData['id'] = $oJob->id;
                $nestedData['car_plates'] = $oJob->car_plates;
                $nestedData['car_brand'] = $oJob->Car->brand;
                $nestedData['car_version'] = $oJob->Car->version;
                $nestedData['car_model'] = $oJob->car_model;

                $nestedData['status_id'] = view("admin.ViewsTools.buttons", ['status' => $oJob->status_id, 'name' => $oJob->Status->name])->render();
                $nestedData['options'] = view("admin.ViewsTools.options-job", ['edit' => $edit, 'delete' => $delete, 'details' => $details, 'note' => $note, 'repair' => $repair])->render();

                $data[] = $nestedData;
            }
        }

        $json_data = array(
            'draw'              => intval($request->input('draw')),
            'recordsTotal'      => intval($totalData),
            'recordsFiltered'   => intval($totalFiltered),
            'data'              => $data
        );

        return $json_data;
    }
        
    public function getAdd()
    {
        $oCar = CarModel::where('active', 1)->get();
        $oStatus = StatusModel::get();
        $oParts = CarPartModel::get();

        return view("admin.{$this->sNameModule}.add", [
            'iIdModule' => $this->iIdModule,
            'sNameModule' => $this->sNameModule,
            'sNameTitle' => $this->rowModule->title,
            'oCar' => $oCar,
            'oStatus' => $oStatus,
            'oParts' => $oParts,
        ]);
    }

    public function postAdd(Request $request)
    {
        $respuesta = array(
            'error' => 'success',
            'mensaje' => ''
        );

        $datos = $request->input();

        try
        {
            if(!isset($datos['check_in']))
            {
                $respuesta['error'] = 'error';
                $respuesta['mensaje'] = 'CheckIn vacio!';
                return $respuesta;
            }

            DB::beginTransaction();

            $newJobEntry = new JobEntryModel();

            $newJobEntry->customer_name = $datos['customer_name'];
            $newJobEntry->customer_number = $datos['customer_number'];
            $newJobEntry->customer_email = $datos['customer_email'];
            $newJobEntry->car_id = $datos['car_id'];
            $newJobEntry->car_model = $datos['car_model'];
            $newJobEntry->car_plates = $datos['car_plates'];
            $newJobEntry->failures = $datos['failures'];
            $newJobEntry->status_id = 1;

            $checkIn = array();

            foreach($datos['check_in'] as $key => $check)
            {
                array_push($checkIn, $key);
            }
    
            $newJobEntry->check_in = json_encode($checkIn);


           if(isset($datos['car_parts']))
            {
                $oFiles = $request->file('car_parts');
                $car_damage = array();

                for($i=0; $i<count($datos['car_parts']); $i++)
                {
                    $sExtensionFile = $oFiles[$i]['image']->getClientOriginalExtension();
                    $sName = JobEntryController::generateString(10).".".$sExtensionFile;

                    $sPathToSave = $datos['car_plates'];

                    array_push($car_damage, ['part' => $datos['car_parts'][$i]['name'], 'image' => $sName]);

                    $result = Storage::disk('checkIn')->putFileAs($sPathToSave, $oFiles[$i]['image'], $sName);
                }    

                $newJobEntry->car_damage = json_encode($car_damage);
            }

            $newJobEntry->save();

            $newResposible = new ResponsibleModel();

            $newResposible->user_id = Auth::user()->id;
            $newResposible->action = 'create';
            $newResposible->module_id = $this->iIdModule;
            $newResposible->data_id = $newJobEntry->id;

            $newResposible->save();

            DB::commit();

            return $respuesta;
        }
        catch(QueryException $e)
        {
            DB::rollback();
            $respuesta['error'] = 'error';
            $respuesta['mensaje'] = 'Error al guardar en la base de datos!';
            \Log::error($e->getMessage());
            return $respuesta;
        }
        catch(Exception $e)
        {
            DB::rollback();
            $respuesta['error'] = 'error';
            $respuesta['mensaje'] = 'Error no controlado!';
            \Log::error($e->getMessage());
            return $respuesta;
        }
    }

    public function getEdit($id)
    {
        try
        {
            $oJob = JobEntryModel::where('id', $id)->firstOrFail();
            $oCars = CarModel::where('active', 1)->get();
            $oParts = CarPartModel::get();

            if($oJob->status_id > 1)
            {
                return redirect()->route('job-entrysIndex');
            }

            $car_damage = json_decode($oJob->car_damage);
            $aCheckIn = json_decode($oJob->check_in);

            return view("admin.{$this->sNameModule}.edit", [
                'iIdModule' => $this->iIdModule,
                'sNameModule' => $this->sNameModule,
                'sNameTitle' => $this->rowModule->title,
                'oJob' => $oJob,
                'oCars' => $oCars,
                'oParts' => $oParts,
                'car_damage' => $car_damage,
                'aCheckIn' => $aCheckIn,
            ]);
        }
        catch(Exception $e)
        {
            abort(404, 'El Registro no existe en la base de datos');
        }
    }

    public function postEdit(Request $request, $id)
    {
        $respuesta = array(
            'error' => 'success',
            'mensaje' => ''
        );

        $datos = $request->input();

        try
        {
            $oJobEntry = JobEntryModel::where('id', $id)->firstOrFail();

            if(!isset($datos['check_in']))
            {
                $respuesta['error'] = 'error';
                $respuesta['mensaje'] = 'CheckIn vacio!';
                return $respuesta;
            }

            if(!isset($datos['car_parts']))
            {
                $oJobEntry->car_damage = null;
                Storage::disk('checkIn')->deleteDirectory($oJobEntry->car_plates);
            }

            if(!isset($oJobEntry->car_damage))
            {
                $oFiles = $request->file('car_parts');
                $car_damage = array();

                for($i=0; $i<count($datos['car_parts']); $i++)
                {
                    $sExtensionFile = $oFiles[$i]['image']->getClientOriginalExtension();
                    $sName = JobEntryController::generateString(10).".".$sExtensionFile;

                    $sPathToSave = $datos['car_plates'];

                    array_push($car_damage, ['part' => $datos['car_parts'][$i]['name'], 'image' => $sName]);

                    $result = Storage::disk('checkIn')->putFileAs($sPathToSave, $oFiles[$i]['image'], $sName);
                }    

                $oJobEntry->car_damage = json_encode($car_damage);
            }
            else if(isset($datos['car_parts']))
            { 
                $aCar_damage = json_decode($oJobEntry->car_damage);
                $oFiles = $request->file('car_parts');
                $car_damage = array();

                //Borrar Elementos
                for($i=0; $i<count($aCar_damage); $i++)
                {
                    if(isset($datos['car_parts'][$i]['name']))
                    {
                        if($aCar_damage[$i]->part != $datos['car_parts'][$i]['name'])
                        {
                            Storage::disk('checkIn')->delete($oJobEntry->car_plates.'/'.$aCar_damage[$i]->image);
                            array_splice($aCar_damage, $i, 1);
                            $i--;
                        }
                    }
                    else
                    {
                        Storage::disk('checkIn')->delete($oJobEntry->car_plates.'/'.$aCar_damage[$i]->image);
                        array_splice($aCar_damage, $i, 1);
                        $i--;
                    }
                }

                $oJobEntry->car_damage = json_encode($aCar_damage);
                
                //Mover archivos a nueva carpeta
                if($oJobEntry->car_plates !== $datos['car_plates'])
                {
                    $directory = $oJobEntry->car_plates;
                    $files = Storage::disk('checkIn')->allFiles($directory);
                    foreach($files as $file)
                    {
                        $pos = strpos($file,'/');
                        $fileName = substr($file, $pos+1);
                        
                        Storage::disk('checkIn')->makeDirectory($datos['car_plates']);
                        Storage::disk('checkIn')->move($file, $datos['car_plates'].'/'.$fileName);
                        Storage::disk('checkIn')->deleteDirectory($oJobEntry->car_plates);
                    }
                }

                $sPathToSave = $datos['car_plates'];
                $aCar_damage = json_decode($oJobEntry->car_damage);

                foreach($datos['car_parts'] as $key => $part)
                {
                    if(!isset($oFiles[$key]))
                    {
                        continue;
                    }

                    if(!isset($aCar_damage[$key]))
                    {
                        $sExtensionFile = $oFiles[$key]['image']->getClientOriginalExtension();
                        $sName = JobEntryController::generateString(10).".".$sExtensionFile;

                        array_push($aCar_damage, ['part' => $part['name'], 'image' => $sName]);
                        $result = Storage::disk('checkIn')->putFileAs($sPathToSave, $oFiles[$key]['image'], $sName);

                        continue;
                    }

                    if($aCar_damage[$key]->part !== $part['name']) //En caso de que se edite el nombre de algun daño antes declarado
                    {
                        $aCar_damage[$key]->part = $part['name'];

                        if(array_key_exists($key, $oFiles))
                        {
                            $sExtensionFile = $oFiles[$key]['image']->getClientOriginalExtension();
                            $sName = JobEntryController::generateString(10).".".$sExtensionFile;
                            Storage::disk('checkIn')->delete($sPathToSave."/".$aCar_damage[$key]->image);
                            $aCar_damage[$key]->image = $sName;
                            $result = Storage::disk('checkIn')->putFileAs($sPathToSave, $oFiles[$key]['image'], $sName);
                        }

                        continue;
                    }

                    if(array_key_exists($key, $oFiles))
                    {
                        $sExtensionFile = $oFiles[$key]['image']->getClientOriginalExtension();
                        $sName = JobEntryController::generateString(10).".".$sExtensionFile;
                        Storage::disk('checkIn')->delete($sPathToSave."/".$aCar_damage[$key]->image);
                        $aCar_damage[$key]->image = $sName;
                        $result = Storage::disk('checkIn')->putFileAs($sPathToSave, $oFiles[$key]['image'], $sName);
                    }
                }

                $oJobEntry->car_damage = json_encode($aCar_damage);
            }

            $checkIn = array();

            foreach($datos['check_in'] as $key => $check)
            {
                array_push($checkIn, $key);
            }
            
            $oJobEntry->check_in = json_encode($checkIn);
           
            $oJobEntry->car_id = $datos['car_id'];
            $oJobEntry->customer_name = $datos['customer_name'];
            $oJobEntry->customer_number = $datos['customer_number'];
            $oJobEntry->customer_email = $datos['customer_email'];
            $oJobEntry->car_plates = $datos['car_plates'];
            $oJobEntry->car_model = $datos['car_model'];

            $oJobEntry->save();

            $newResposible = new ResponsibleModel();

            $newResposible->user_id = Auth::user()->id;
            $newResposible->action = 'edit';
            $newResposible->module_id = $this->iIdModule;
            $newResposible->data_id = $oJobEntry->id;

            $newResposible->save();

            DB::commit();

            return $respuesta;
        }
        catch(QueryException $e)
        {
            DB::rollback();
            $respuesta['error'] = 'error';
            $respuesta['mensaje'] = 'Error al guardar en la base de datos!';
            \Log::error($e->getMessage());
            return $respuesta;
        }
        catch(Exception $e)
        {
            DB::rollback();
            $repuesta['error'] = 'error';
            $respuesta['mensaje'] = 'Error no controlado';
            \Log::error($e->getMessage());
            return $respuesta;
        }
    }

    public function getDelete($id)
    {
        $respuesta = array(
            'error' => 'success',
            'mensaje' => ''
        );

        try
        {
            $oJobEntry = JobEntryModel::where('id', $id)->firstOrFail();

            Storage::disk('checkIn')->deleteDirectory($oJobEntry->car_plates);
            Storage::disk('repairs')->deleteDirectory($oJobEntry->car_plates);

            DB::beginTransaction();

            $oJobEntry->delete();

            DB::commit();

            return $respuesta;
        }
        catch(Exception $e)
        {
            DB::rollback();
            $respuesta['error'] = 'error';
            $respuesta['mensaje'] = 'Error no controlado!';
            \Log::error($e->getMessage());
            return $respuesta;
        }
    }


    //Funcion para generar string aleatorio de n numero de caracteres
    static function generateString ($length){
        $string = "";
        $possible = "0123456789bcdfghjkmnpqrstvwxyz";
        $i = 0;
        while ($i < $length) {
          $char = substr($possible, mt_rand(0, strlen($possible)-1), 1);
          $string .= $char;
          $i++;
        }
        return $string;
    }
}

